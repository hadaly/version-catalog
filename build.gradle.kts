import org.jetbrains.changelog.date

plugins {
    alias(libs.plugins.androidGitVersion)
    alias(libs.plugins.changelog)
    `maven-publish`
    `version-catalog`
}

androidGitVersion {
    format = "%tag%"
    hideBranches = listOf("master", "develop")
}

catalog {
    versionCatalog {
        from(files("gradle/libs.versions.toml")) to "fr.hadaly.catalog:kmm:${androidGitVersion.name()}"
    }
}

publishing {
    publications {
        create<MavenPublication>("tomlFile") {
            from(components["versionCatalog"])
            groupId = "fr.hadaly.catalog"
            artifactId = "kmm"
            version = androidGitVersion.name()
        }
    }
    repositories {
        maven {
            url = uri("https://gitlab.com/api/v4/projects/40515787/packages/maven")
            name = "GitLab"
            credentials {
                username = System.getenv("DEPLOY_USER")
                password = System.getenv("DEPLOY_PWD")
            }
        }
    }
}

changelog {
    version.set(androidGitVersion.name())
    path.set("${project.projectDir}/CHANGELOG.md")
    header.set(provider { "[${version.get()}] - ${date()}" })
    itemPrefix.set("-")
    keepUnreleasedSection.set(true)
    unreleasedTerm.set("[Unreleased]")
    groups.set(listOf("Added", "Changed", "Deprecated", "Removed", "Fixed", "Security"))
}

tasks.register("prepareChangelog") {
    dependsOn("patchChangelog")
    doLast {
        exec {
            commandLine("git", "commit", "--amend", "-a", "--no-edit")
        }
        exec {
            commandLine("git", "tag", "${androidGitVersion.name()}", "-f", "HEAD")
        }
    }
}
