# Version catalog for Android KMM projects

## Do a release

1. Update the ["Unreleased"] section of the CHANGELOG.
2. Commit the changes 
3. Tag the commit with the wanted release version
4. Launch "prepareChangelog" tasks
5. Push the tag to the repository. Done !
