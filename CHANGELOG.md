# Changelog

## [Unreleased]

### Added

### Changed

### Deprecated

### Removed

### Fixed

### Security

## [1.2.0] - 2023-01-22

### Changed
- Update to Kotlin 1.8
- Updated AndroidX until January 2023
- Updated various libs to stable releases

### Security
- Updated Protobuf to latest as CVE existed for previous versions.

## 1.1.8 - 2022-12-18

### Changed
- Updated project changelog configuration

## 1.1.7 - 2022-12-18

### Changed
- Update Ktor to 2.2.1

## 1.1.6 - 2022-12-18

### Added
- Androidx Splashscreen

## 1.1.5 - 2022-12-18

### Changed
- Update Koin releases

## 1.1.4 - 2022-12-17

### Added
- Paparazzi
- Orbit MVI
- AndroidX Splashscreen

### Changed
- Jetbrains Changelog -> 2.0.0
- Koin Android -> 3.3.0
- Decompose -> 1.0.0-beta02
- Jetbrains Compose -> 1.3.0-rc01
- Latest Compose stable updates (BOM 2022-12-00)
- Latest AndroidX stable updates (7/12/2022)

## 1.0.0 - 2022-09-11

### Added
- Initial release
